import { storeData } from './exe'

const modLogin = (state = {}, action) => {
	// handle error server not ready
	if (action.request !== undefined) {
		if (action.request.error !== undefined) {
			state = {
				errMsg: action.request.error
			}
			return Object.assign({}, state);
		}
	}

	// state = {}

	switch (action.type) {
		case 'RESETALL' :
			return {};
			
		case 'LOGIN' :
			let login = {
				success: 0,
				email: action.email
			}

			if (action.request.status === "success") {
				login.success = 1

				if (action.request.flag == 0) {
					Object.assign(login, {msg: "We have sent OTP."})
				} else {
					Object.assign(login, {msg: "Registration success. Please verify your email."})
				}

				Object.assign(login, {flag: action.request.flag})
			}
			else {
				if (action.request.messages !== undefined) {
					Object.assign(login, {msg: action.request.messages[0]})
				}

				if (action.request.email !== undefined) {
					Object.assign(login, {msg: action.request.email[0]})
				}
			}

			return Object.assign({}, state, {login: login});
		
		case 'OTP' :
			let token  = ""

			if (action.request.token !== undefined) {
				token = action.request.token

				storeData('token', token)
			}

			return Object.assign({}, state, {token: token});

		case 'ME' :
			let info  = 0

			if (action.request.error === undefined) {
				info = 1
			}

			return Object.assign({}, state, {valid_token: info });

		case 'MYDATA' :
			if (action.request.status === "success") {
				let jmlPass = action.request.data.keyPass.length
				let level   = ""

				if (jmlPass > 0 && jmlPass <= 5) {
					level = "Poor"
				}

				if (jmlPass > 5 && jmlPass <= 10) {
					level = "Good"
				}

				if (jmlPass > 10 && jmlPass <= 100) {
					level = "Awesome"
				}

				if (jmlPass > 100 && jmlPass <= 500) {
					level = "Awsu"
				}

				if (jmlPass > 500 ) {
					level = "Jancoooqq"
				}

				storeData('me', JSON.stringify(action.request.data))
				return Object.assign({}, state, {mydata: action.request.data, level: level });
			}

			return Object.assign({}, state)

		default:
			return state;
	}
}

export default modLogin;