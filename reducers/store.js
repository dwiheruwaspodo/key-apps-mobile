import { createStore, combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import modLogin from './red-login'
import modKey from './red-key'

let kumpulanReducer = combineReducers({
	form: formReducer,
	modLogin: modLogin,
	modKey:modKey
});

let store = createStore(kumpulanReducer);

export default store;
