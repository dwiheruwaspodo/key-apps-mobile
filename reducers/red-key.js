import { storeData } from './exe'

const modKey = (state = {}, action) => {
	// handle error server not ready
	if (action.request !== undefined) {
		if (action.request.error !== undefined) {
			state = {
				errMsg: action.request.error
			}
		} 
	}

	switch (action.type) {
		case 'RESETALL' :
			return {};
		break
		case 'ADD_KEY' :		
			return Object.assign({}, state, {addKey: action.request});
		break
		case 'DELETE_KEY' :		
			return Object.assign({}, state, {deleteKey: action.request})
		break
		case 'RESET_KEY' :
			return Object.assign({}, state, {
				addKey: [],
				deleteKey: []
			})
		default:
			return state;
	}
}

export default modKey;