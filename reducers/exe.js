import { AsyncStorage } from "react-native"
import { config } from './config';

export const get = (url, auth=null) => {
    url = config.baseUrl + "api/" + url;
    
    return fetch(url, {
        method: 'GET',
        // mode : 'no-cors',
        headers: {
            'Content-Type': 'application/json',
            'Accept' : 'application/json',
            'Authorization' : auth,
        }
    }).then((response) => response.json())
    .then((responseJson) => {
      return responseJson;
    })
    .catch((error) => {
        return {
            error: error.message
        }
    })
}

// post
export const post = (url, post, auth=null) => {
    url = config.baseUrl + "api/" + url;

    return fetch(url, {
        // mode: "no-cors",
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept' : 'application/json',
            'Authorization' : auth,
        },
        body: JSON.stringify(post)
    })
    .then((response) => response.json())
    .then((responseJson) => {
      return responseJson;
    })
    .catch((error) => {
        return {
            error: error.message
        }
    })
}

// store to storage
export const storeData = async (key, value) => {
  try {
    const saving = await AsyncStorage.setItem(key, value)

    if (saving) {
        return true
    }

    return false

  } catch (error) {
    return false
  }
}