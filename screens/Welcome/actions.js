import store from '../../reducers/store';
import { post, get } from '../../reducers/exe';

// gologin
export const goLogin = (param) => {
	post('check-user', param).then(function(res) {
		let result = res
		store.dispatch({
			type    : 'LOGIN',
			email   : param.email,
			request : result
		})
	})
}

export const goOTP = (param) => {
	post('login', param).then(function(res) {
		let result = res
		store.dispatch({
		    type: 'OTP',
		    request: result
		})
	})
}

export const whoIam = (auth) => {
	get('me', auth).then(function(res) {
		let result = res
		store.dispatch({
		    type: 'ME',
		    request: result
		})
	})
}

export const myData = (auth) => {
	get('my-pass', auth).then(function(res) {
		let result = res
		store.dispatch({
		    type: 'MYDATA',
		    request: result
		})
	})
}

export const resetAll = () => {
	store.dispatch({
	    type: 'RESETALL',
	    request: 'RESETALL'
	})
}



