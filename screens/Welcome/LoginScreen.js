import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { connect } from 'react-redux';
import { Container, Content, Button, Item, Input, Toast } from 'native-base';
import { WebBrowser } from 'expo';
import { MonoText } from '../../components/StyledText';
import { goOTP } from "./actions";

class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor() {
    super()
  }

  shouldComponentUpdate(nextProps, nextState) {    
    let token = nextProps.otpProps.modLogin.token

    if (token !== undefined) {
      if (token !== "") {
          nextProps.navigation.navigate('Main')
          return true
      }
      
      Toast.show({
        text: "Wrong password!",
        buttonText: "Ok",
        duration: 3000
      })

      return false;
    }

    return false;
  }

  render() {
    
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <View style={styles.welcomeContainer}>
            <Image
              source={
                __DEV__
                  ? require('../../assets/images/robot-prod.png')
                  : require('../../assets/images/robot-dev.png')
              }
              style={styles.welcomeImage}
            />
          </View>

          <View style={styles.getStartedContainer}>

            <Text style={styles.getStartedText}>Login</Text>

            <View style={[styles.codeHighlightContainer, styles.homeScreenFilename]}>
              <MonoText style={styles.codeHighlightText}>We have sent OTP to your email </MonoText>
            </View>

            <Text style={styles.getStartedText}>
              Please input OTP bellow
            </Text>

            <ComponentInput { ...this.props } />
          </View>

          
        </ScrollView>

        <View style={styles.tabBarInfoContainer}>
          <Text style={styles.tabBarInfoText}>Key Pass Application</Text>

          <View style={[styles.codeHighlightContainer, styles.navigationFilename]}>
            <MonoText style={styles.codeHighlightText}>Myles Waspodov</MonoText>
          </View>
        </View>
      </View>
    );
  }
}

class ComponentInput extends React.Component {
  constructor() {
    super()
    this.state = {
      otp: '',
    }
  }

  _changeOTP(value) {
    this.setState({
      otp: value
    });
  }

  _submitOTP() {
    goOTP({
      email: this.props.otpProps.modLogin.login.email,
      password: this.state.otp
    })
  }

  render() {
    return(
        <View style={styles.getStartedContainer}>
          <Item regular style={{ marginTop: 20 }}>
            <Input placeholder="OTP" value={ this.state.otp } onChangeText={ this._changeOTP.bind(this) } />
          </Item>

          <Button block info style={{ marginTop: 10 }} onPress={ this._submitOTP.bind(this) }>
            <Text style={{ color: "white" }}> Send </Text>
          </Button>
        </View>
    );
  }
}

const mapStateToProps = (state) => {
    // console.log(state);
    return { otpProps: state }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 20,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
})

export default connect(mapStateToProps)(LoginScreen);