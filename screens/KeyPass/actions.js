import store from '../../reducers/store';
import { post, get } from '../../reducers/exe';

// create key
export const createKey = (param, auth) => {
	post('keypass/create', param, auth).then(function(res) {
		let result = res
		store.dispatch({
		    type: 'ADD_KEY',
		    request: result
		})
	})
}

// delete key
export const deleteKey = (param, auth) => {
	post('keypass/delete', param, auth).then(function(res) {
		let result = res
		store.dispatch({
		    type: 'DELETE_KEY',
		    request: result
		})
	})
}

export const resetKey = () => {
	store.dispatch({
	    type: 'RESET_KEY'
	})	
}




