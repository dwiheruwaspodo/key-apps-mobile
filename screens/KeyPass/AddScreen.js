import React, { Component } from 'react';
import { Container, Header, Content, Form, Item, Input, Label, Button, Toast } from 'native-base';
import { View, AsyncStorage, ScrollView, StyleSheet, Platform, Text } from 'react-native';
import { MonoText } from '../../components/StyledText';
import AddScreenForm from './AddScreenForm';
import { connect } from 'react-redux';
import { createKey, resetKey } from './actions';
import { reset } from 'redux-form';
import store from '../../reducers/store';

class AddScreen extends React.Component {
  static navigationOptions = {
    title: 'Add Key and Password',
  };

  _sendToApi = async (value) => {
    let token = await AsyncStorage.getItem('token')
    await createKey(value, token)
    // reset form
    store.dispatch(reset('addForm'));
  }

  _submitKey = () => {
    // console.log(this.props.addProps.form.addForm)
    if (this.props.addProps.form.addForm.values === undefined || this.props.addProps.form.addForm.syncErrors !== undefined) {
      Toast.show({
        text: "Please fill in the blank",
        buttonText: "OK",
        duration: 3000
      })

      return false
    }

    this._sendToApi(this.props.addProps.form.addForm.values)
  }

  _resultApi = (nextProps, nextState) => {
    if (nextProps.addProps.modKey.addKey !== undefined) {
      if (nextProps.addProps.modKey.addKey.status === "success") {
        alert("Success add new key")
        return
      }
      alert("Failed add new key")
      return
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    this._resultApi(nextProps, nextState)
    return true
  }

  render() {
    console.log(this.props)
    return(    	
  	  	<AddScreenForm submitKey={ this._submitKey.bind(this)} />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  }
});

const mapStateToProps = (state) => {
    // console.log(state);
    return { addProps: state }
}
export default connect(mapStateToProps)(AddScreen);