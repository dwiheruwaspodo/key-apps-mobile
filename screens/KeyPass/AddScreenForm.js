import React, { Component } from 'react';
import { ExpoConfigView } from '@expo/samples';
import { Container, Header, Content, Form, Item, Input, Label, Button } from 'native-base';
import { View, AsyncStorage, ScrollView, StyleSheet, Platform, Text } from 'react-native';
import { MonoText } from '../../components/StyledText';
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux';

class AddScreenForm extends React.Component {
  render() {
    return(	
	  	<Container>
        <Content>
          <View style={styles.getStartedContainer}>
              <Form>
                <Field name="key" placeholder="Key" required id="key" component={inputan} />
                <Field name="username" placeholder="Username" required id="username" component={inputan} />
                <Field name="password" placeholder="Password" required id="password" component={inputan} />
                <Button block info style={{ marginTop: 30 }} onPress={ this.props.submitKey }>
                  <Text style={{ color: "white" }}> Add </Text>
                </Button>
          	      		  	
              </Form>
          </View>
        </Content>
	  	</Container>
    )
  }
}

const validate = values => {
  const errors = {}
    
    if (!values.key) {
        errors.key = 'Key is required!'
    } else if (values.key.length < 1) {
        errors.key = 'Key must be 1 characters or more!'
    }

    if (!values.username) {
        errors.username = 'Username is required!'
    } 
    
    if (!values.password) {
        errors.password = 'Password is required!'
    }
    
    return errors
}
const inputan = ({input, placeholder, meta: { touched, error } }) => (
  <View>
    <Item regular style={{ marginTop: 10 }}>
      <Input 
        placeholder={placeholder} 
        {...input}
      />
    </Item>
    {touched && (error && <Text style={ styles.errorValidate }>{error}</Text>)}
  </View>
)

const styles = StyleSheet.create({
  tabBarInfoContainer: {
    top: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  getStartedContainer: {
    marginLeft: 20,
    marginRight: 20,
  },
  errorValidate: {
    'color': 'red',
    'fontSize': 10
  }
});

export default reduxForm({
  form: 'addForm',  // a unique identifier for this form
  validate          // <--- validation function given to redux-form
})(AddScreenForm)