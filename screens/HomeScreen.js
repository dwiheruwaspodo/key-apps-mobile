import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  RefreshControl,
} from 'react-native';

import { WebBrowser } from 'expo';
import { connect } from 'react-redux';
import { MonoText } from '../components/StyledText';
import { AsyncStorage } from 'react-native'
import { myData, whoIam } from './Welcome/actions'

class HomeScreen extends React.Component {
  
  constructor() {
    super();
    
    this.state = {
      refreshing: false,
      timeNow: "",
      greeting: "",
      level: "",
      user: {}
    };
  }

  componentWillMount() {
    this._greetings()
    this._getMyData()
    this._getWhoIam()
  }

  _getMyData = async () => {
    const token = await AsyncStorage.getItem('token')
    await myData(token)
  }

  _getWhoIam = async () => {
    let user = await AsyncStorage.getItem('me')
    user = JSON.parse(user)

    this.setState({
      user: user
    })
  }

  _greetings = () => {
    let waktuNow = new Date();

    var curHr = waktuNow.getHours();
    var salam = "";

    if (curHr < 12) {
      salam = "Good Morning";
    } else if (curHr < 18) {
      salam = "Good Afternoon";
    } else {
      salam = "Good Evening";
    }

    // set state
    this.setState({
      timeNow: waktuNow,
      greeting: salam
    });
  }

  _onRefresh = async () => {
    const token = await AsyncStorage.getItem('token')
    this.setState({
      refreshing: true
    })

    whoIam(token)

    this.setState({
      refreshing: false
    })
  }

  _validationLogin(nextProps, nextState) {
    let indicate = nextProps.homeProps.modLogin.login

    if (indicate !== undefined) {
      if (indicate.success === 0 && indicate.flag === 0) {
          nextProps.navigation.navigate('Login')
          return true
      }

      if (nextProps.homeProps.modLogin.login.msg !== undefined) {
        alert(nextProps.homeProps.modLogin.login.msg)
      }
    }

    return false
  }

  shouldComponentUpdate(nextProps, nextState) {
    this._validationLogin(nextProps, nextState)
    return true
  }

  static navigationOptions = {
    header: null,
  };

  render() {
    console.log(this.props)
    let level = ""

    if (this.props.homeProps.modLogin.level !== undefined) {
      level = this.props.homeProps.modLogin.level
    }

    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer} refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh.bind(this)}
          />
        }>
          
          <View style={styles.getStartedContainer}>

            <Text style={styles.getStartedText}>{ this.state.greeting }</Text>

            <Text style={styles.getStartedText}>
              Save your brain to remind something
            </Text>
          </View>

          <View style={styles.welcomeContainer}>
            <Image
              source={
                __DEV__
                  ? require('../assets/images/robot-dev.png')
                  : require('../assets/images/robot-prod.png')
              }
              style={styles.welcomeImage}
            />
          </View>


          <View style={styles.helpContainer}>
            <TouchableOpacity onPress={this._handleHelpPress} style={styles.helpLink}>
              <Text style={styles.helpLinkText}>Hey, { this.state.user.email }! </Text>
            </TouchableOpacity>
          </View>

        </ScrollView>

        <View style={styles.tabBarInfoContainer}>
          <Text style={styles.tabBarInfoText}>
            Your Level Reminder
          </Text>

          <View style={[styles.codeHighlightContainer, styles.navigationFilename]}>
            <MonoText style={styles.codeHighlightText}>
              { level }
            </MonoText>
          </View>
        </View>
      </View>
    );
  }



  _handleHelpPress = () => {
    WebBrowser.openBrowserAsync(
      'https://docs.expo.io/versions/latest/guides/up-and-running.html#can-t-see-your-changes'
    );
  };
}

const mapStateToProps = (state) => {
    // console.log(state);
    return { homeProps: state }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    marginTop: 200,
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});

export default connect(mapStateToProps)(HomeScreen);