import React, { Component } from 'react';
import { ListView, Platform, View, AsyncStorage, ScrollView, StyleSheet } from 'react-native';
import { MonoText } from '../components/StyledText';
import { Container, Header, Content, Card, CardItem, Text, Body } from "native-base";
import { connect } from 'react-redux';

class DetailScreen extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.state.params.key,
    };
  };

  render() {
    
    let data = this.props.navigation.state.params

    return (
      <Container>
        <Content padder>
          <Card>
            <CardItem header bordered>
              <Text>Username  </Text>
            </CardItem>
            <CardItem bordered>
              <Body>
                <Text>
                  { data.username }
                </Text>
              </Body>
            </CardItem>
            <CardItem footer>
              
            </CardItem>
            <CardItem header bordered>
              <Text>Password  </Text>
            </CardItem>
            <CardItem bordered>
              <Body>
                <Text>
                  { data.password }
                </Text>
              </Body>
            </CardItem>
            
          </Card>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  tabBarInfoContainer: {
    top: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
});

const mapStateToProps = (state) => {
    return { detailProps: state }
}

export default connect(mapStateToProps)(DetailScreen);