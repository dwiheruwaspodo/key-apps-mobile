import React, { Component } from 'react';
import { ListView, Platform, View, AsyncStorage, ScrollView, StyleSheet } from 'react-native';
import { MonoText } from '../components/StyledText';
import { Container, Content, Button, Icon, List, ListItem, Text, Toast } from 'native-base';
import { connect } from 'react-redux';
import { myData, whoIam } from './Welcome/actions'
import { deleteKey, resetKey } from './KeyPass/actions'

class LinksScreen extends React.Component {
  static navigationOptions = {
    title: 'List',
  };

  constructor() {
    super();
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      basic: true
    };
  }

  componentWillMount() {
    this._getMyData()
    this._getWhoIam()
  }

  _getMyData = async () => {
    const token = await AsyncStorage.getItem('token')
    await myData(token)
  }

  _getWhoIam = async () => {
    let user = await AsyncStorage.getItem('me')
  }

  _validationLogin(nextProps, nextState) {
    let indicate = nextProps.listProps.modLogin.login

    if (indicate !== undefined) {
      if (indicate.success === 1 && indicate.flag === 0) {
          nextProps.navigation.navigate('Login')
          return true
      }

      if (nextProps.listProps.modLogin.login.msg !== undefined) {
        alert(nextProps.listProps.modLogin.login.msg)
      }
    }

    return false
  }

  _alertDelete = (nextProps, nextState) => {
    if (nextProps.listProps.modKey.deleteKey !== undefined) {
      if (nextProps.listProps.modKey.deleteKey.status === "success") {
        Toast.show({
          text: "Key has been deleted.",
          buttonText: "OK",
          duration: 3000
        })
      }  
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    this._validationLogin(nextProps, nextState)
    this._alertDelete(nextProps, nextState)
    return true
  }

  _deleteRow = async (data, secId, rowId, rowMap) => {
    rowMap[`${secId}${rowId}`].props.closeRow();
    const token = await AsyncStorage.getItem('token')
    await deleteKey(data, token)
    await myData(token)
  }

  render() {
    // console.log(this.props)
    let level = ""

    if (this.props.listProps.modLogin.level !== undefined) {
      level = this.props.listProps.modLogin.level
    }

    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

    let keyuser = {}

    if (this.props.listProps.modLogin.mydata !== undefined) {
      keyuser = this.props.listProps.modLogin.mydata.keyPass
    }

    return (
      <ScrollView style={styles.container}>
        <HeaderList level={ level } />
          <List
            leftOpenValue={75}
            rightOpenValue={-75}
            dataSource={this.ds.cloneWithRows(keyuser)}
            renderRow={ (data, sectionID, rowID, rowMap) =>
              <ListItem>
                <Text style={{ marginLeft: 20}} >{ parseInt(rowID)+1 }. { data.key } </Text>
              </ListItem>}
            renderLeftHiddenRow={(data) =>
              <Button full onPress={() => this.props.navigation.navigate('Detail', data) }>
                <Icon active name="information-circle" />
              </Button>}
            renderRightHiddenRow={(data, secId, rowId, rowMap) =>
              <Button full danger onPress={() => this._deleteRow(data, secId, rowId, rowMap) }>
                <Icon active name="trash" />
              </Button>}
          />
      </ScrollView>
    );
  }
}

class HeaderList extends Component {
  constructor() {
    super()
  }

  render() {
    return(
      <View style={styles.tabBarInfoContainer}>
        <Text style={styles.tabBarInfoText}>
          Hey, { this.props.level }!
        </Text>

        <View style={[styles.codeHighlightContainer, styles.navigationFilename]}>
          <MonoText style={styles.codeHighlightText}>
            List Your Key and Password
          </MonoText>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  tabBarInfoContainer: {
    top: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
});

const mapStateToProps = (state) => {
  // console.log(state)
    return { listProps: state }
}

export default connect(mapStateToProps)(LinksScreen);