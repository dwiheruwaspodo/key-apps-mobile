import React from 'react';
import { createSwitchNavigator, createStackNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import LoginScreen from '../screens/Welcome/LoginScreen';
import WelcomeScreen from '../screens/Welcome/WelcomeScreen';


const AppLogin = createStackNavigator({
    Welcome: WelcomeScreen,
    Login: LoginScreen,
});


export default createSwitchNavigator({
	// You could add another route here for authentication.
  	// Read more at https://reactnavigation.org/docs/en/auth-flow.html
	Main     : MainTabNavigator,
	AppLogin : AppLogin	
},{
    initialRouteName: 'AppLogin',
});